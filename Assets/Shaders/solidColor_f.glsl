#version 420

// Fragment Shader Inputs
in VertexData
{
	vec3 normal;
	vec3 texCoord;
	vec4 colour;
	vec3 posEye;
} vIn;

// Multiple render targets!
layout(location = 0) out vec4 FragColor;

void main()
{
	// This currently just sets the outline color to black, try controlling the outline
	// color using a uniform
	FragColor = vec4(0,0,0,1);
}