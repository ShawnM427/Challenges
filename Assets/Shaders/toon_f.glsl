#version 420

uniform vec4 u_lightPos;
uniform vec4 u_colour;

// Note: Uniform bindings
// This lets you specify the texture unit directly in the shader!
layout(binding = 0) uniform sampler2D u_rgb; // rgb texture

// Fragment Shader Inputs
in VertexData
{
	vec3 normal;
	vec3 texCoord;
	vec4 colour;
	vec3 posEye;
} vIn;

// Multiple render targets!
layout(location = 0) out vec4 FragColor;

vec3 toon()
{
	// TODO: IMPLEMENT TOON SHADING

	return vec3(1.0);
}

void main()
{
	// The line below is diffuse + emissive
	// Try playing around with these values to see how they change the result
	FragColor = vec4(toon() + u_colour.rgb, 1.0);
}