/*
	Author: Shawn Matthews
	Date:   Feb 7 2018
*/
#pragma once

#include "GLEW/glew.h"
#include "TTK/io.h"

	class RenderTarget {
	public:
		struct BufferInfo {
			GLenum MinFilter;
			GLenum MagFilter;
			GLenum WrapMode;

			GLenum InternalFormat;

			GLenum PixelFormat;
			GLenum PixelType;

			GLenum BindingTarget;
			GLint  MipLevel;

			void  *ClearValue;
			
			BufferInfo(
					GLenum bindTarget,
					GLenum internalFormat = GL_RGBA8,
					GLenum pixelFormat = GL_RGBA,
					GLenum pixelType = GL_UNSIGNED_BYTE,
					GLenum minFilter = GL_LINEAR,
					GLenum magFilter = GL_LINEAR,
					GLenum wrapMode = GL_CLAMP_TO_EDGE,
					int    boundMipLevel = 0,
				    void  *clearData = nullptr) :
				MinFilter(minFilter),
				MagFilter(magFilter),
				WrapMode(wrapMode),
				InternalFormat(internalFormat),
				PixelFormat(pixelFormat),
				PixelType(pixelType),
				BindingTarget(bindTarget),
				MipLevel(boundMipLevel),
				ClearValue(clearData) {}

		private:
			friend class RenderTarget;
			GLuint TextureHandle;
		};

		RenderTarget(uint32_t width, uint32_t height);
		~RenderTarget();

		void Resize(uint32_t width, uint32_t height);

		void Bind() const;
		static void UnBind();

		void Clear() const;
		void BlitDepthToBackBuffer(const uint32_t width, const uint32_t height) const;

		uint32_t NumBuffers() const { return myBufferCount; }

		void BindTexture(GLenum bindLoc, uint8_t location) const;
		void BindTextureByID(uint8_t id, uint8_t location) const;
		void BindDepthTex(uint8_t location) const;

		void AddDepthStencilBuffer();
		void AddBuffer(const BufferInfo& buffer);
		void AddBuffer(
			GLenum bindTarget,
			GLenum internalFormat = GL_RGBA8,
			GLenum pixelFormat = GL_RGBA,
			GLenum pixelType = GL_UNSIGNED_BYTE,
			GLenum minFilter = GL_LINEAR,
			GLenum magFilter = GL_LINEAR,
			GLenum wrapMode = GL_CLAMP_TO_EDGE,
			int    boundMipLevel = 0) {
			AddBuffer(BufferInfo(bindTarget, internalFormat, pixelFormat, pixelType, minFilter, magFilter, wrapMode, boundMipLevel));
		}
		void Finalize();

		bool IsFinalized() const { return isFinalized; }

		uint32_t GetHeight() const { return myHeight; }
		uint32_t GetWidth() const { return myWidth; }

		GLuint GetHandle() const { return myHandle; }

	private:
		uint32_t myWidth;
		uint32_t myHeight;
		GLuint   myHandle;

		bool     isFinalized;

		bool        hasDepthBuffer;
		BufferInfo  myDepthBuffer;

		BufferInfo *myBuffers;
		uint32_t    myBufferCount;

		void __Bind() const;
	};