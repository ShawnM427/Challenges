// Core Libraries
#include <iostream>
#include <string>
#include <math.h>
#include <map> // for std::map
#include <memory> // for std::shared_ptr
#include <fstream>

// 3rd Party Libraries
#define GLEW_STATIC
#include <GLEW\glew.h>
#include <GLUT/freeglut.h>
#include <TTK\OBJMesh.h>
#include <TTK\Camera.h>
#include <TTK\Texture2D.h>
#include <imgui\imgui_impl.h>
#include <glm\vec3.hpp>
#include <glm\gtx\color_space.hpp>

// User Libraries
#include "Shader.h"
#include "ShaderProgram.h"
#include "GameObject.h"
#include "TTK\Utilities.h"
#include "Texture3D.h"
#include "RenderTarget.h"

// Defines and Core variables
#define FRAMES_PER_SECOND 60
const int FRAME_DELAY = 1000 / FRAMES_PER_SECOND; // Milliseconds per frame

int windowWidth = 800;
int windowHeight = 600;

glm::vec3 mousePosition; // x,y,0
glm::vec3 mousePositionFlipped; // x, height - y, 0

// A few conversions to know
const float degToRad = 3.14159f / 180.0f;
const float radToDeg = 180.0f / 3.14159f;

float deltaTime = 0.0f; // amount of time since last update (set every frame in timer callback)

glm::vec3 position;
float movementSpeed = 5.0f;
glm::vec4 lightPos;

bool paused = false;

// Cameras
TTK::Camera playerCamera; // the camera you move around with wasd + mouse

// Asset databases
// A std::map is just like a std::vector, but instead of using an integer to index into the array, you can use a templated type
// In the following maps we use strings as the indices into the arrays
std::map<std::string, std::shared_ptr<TTK::MeshBase>>  meshes;
std::map<std::string, std::shared_ptr<GameObject>>     gameobjects;
std::map<std::string, std::shared_ptr<TTK::Texture2D>> textures;
std::map<std::string, std::shared_ptr<Texture3D>>      luts;

RenderTarget* renderBuffer = nullptr;

// Materials
std::map<std::string, std::shared_ptr<Material>> materials;

// 0 = lambert
// 1 = toon
// 2 = toon + outlines
int mode = 0;

void initializeShaders()
{
	std::string shaderPath = "../../Assets/Shaders/";

	// Load shaders

	// Load Vertex Shaders
	Shader v_default;
	v_default.loadShaderFromFile(shaderPath + "default_v.glsl", GL_VERTEX_SHADER);

	// Load Fragment Shaders
	Shader f_default, f_solidColor, f_toon;
	f_default.loadShaderFromFile(shaderPath + "default_f.glsl", GL_FRAGMENT_SHADER);
	f_solidColor.loadShaderFromFile(shaderPath + "solidColor_f.glsl", GL_FRAGMENT_SHADER);
	f_toon.loadShaderFromFile(shaderPath + "toon_f.glsl", GL_FRAGMENT_SHADER);

	// Default lambert material
	// This is how we access elements in the materials map
	materials["default"] = std::make_shared<Material>();
	materials["default"]->shader->attachShader(v_default); // Vertex Shader
	materials["default"]->shader->attachShader(f_default); // Fragment Shader
	materials["default"]->shader->linkProgram();

	materials["solidColor"] = std::make_shared<Material>();
	materials["solidColor"]->shader->attachShader(v_default);
	materials["solidColor"]->shader->attachShader(f_solidColor);
	materials["solidColor"]->shader->linkProgram();

	materials["toon"] = std::make_shared<Material>();
	materials["toon"]->shader->attachShader(v_default);
	materials["toon"]->shader->attachShader(f_toon);
	materials["toon"]->shader->linkProgram();
}

void loadMeshes()
{
	// Load meshes
	std::string meshPath = "../../Assets/Models/";

	std::shared_ptr<TTK::OBJMesh> floorMesh = std::make_shared<TTK::OBJMesh>();
	std::shared_ptr<TTK::OBJMesh> sphereMesh = std::make_shared<TTK::OBJMesh>();
	std::shared_ptr<TTK::OBJMesh> torusMesh = std::make_shared<TTK::OBJMesh>();
	std::shared_ptr<TTK::OBJMesh> cubeMesh = std::make_shared<TTK::OBJMesh>();
	std::shared_ptr<TTK::OBJMesh> playerMesh = std::make_shared<TTK::OBJMesh>();

	floorMesh->loadMesh(meshPath + "floor.obj");
	sphereMesh->loadMesh(meshPath + "sphere.obj");
	cubeMesh->loadMesh(meshPath + "cube.obj");
	playerMesh->loadMesh(meshPath + "brickbreaker.obj");

	// Note: looking up a mesh by it's string name is not the fastest thing,
	// you don't want to do this every frame, once in a while (like now) is fine.
	// If you need you need constant access to a mesh (i.e. you need it every frame),
	// store a reference to it so you don't need to look it up every time.
	meshes["floor"] = floorMesh;
	meshes["sphere"] = sphereMesh;
	meshes["cube"] = cubeMesh;
	meshes["quad"] = createQuadMesh();
	meshes["player"] = playerMesh;
}

void loadTextures()
{
	// Load textures
	std::string texturesPath = "../../Assets/Textures/";

	luts["warm"] = std::make_shared<Texture3D>(texturesPath + "warm.cube", 1, 1);
	luts["cool"] = std::make_shared<Texture3D>(texturesPath + "cool.cube", 1, 1);
	luts["green"] = std::make_shared<Texture3D>(texturesPath + "jungle.cube", 1, 1);

	// ... try to put a texture on an object
}

void initializeScene()
{
	loadMeshes();
	loadTextures();

	renderBuffer = new RenderTarget(windowWidth, windowHeight);
	renderBuffer->AddBuffer(GL_COLOR_ATTACHMENT0);
	renderBuffer->Finalize();

	playerCamera.cameraPosition = glm::vec3(0.0f, 100.0f, 0.0f);
	playerCamera.forwardVector = glm::vec3(0.0f, -1.0f, 0.0f);
	playerCamera.upVector = glm::vec3(0.0f, 0.0f, 1.0f);
	playerCamera.rightVector = glm::vec3(1.0f, 0.0f, 0.0f);

	// Create objects
	// Get reference to default material (created in initializeShaders())
	auto defaultMaterial = materials["default"];

	gameobjects["floor"] = std::make_shared<GameObject>(glm::vec3(0.0f, 0.0f, 0.0f), meshes["floor"], defaultMaterial);
	gameobjects["floor"]->colour = glm::vec4(0.1f);
	gameobjects["sphere"] = std::make_shared<GameObject>(glm::vec3(0.0f, 5.0f, 0.0f), meshes["sphere"], defaultMaterial);
	
	gameobjects["player"] = std::make_shared<GameObject>(glm::vec3(0.0f, 0.0f, 0.0f), meshes["player"], defaultMaterial);;
	gameobjects["player"]->colour = glm::vec4(1.0f);

	// Set object properties
	gameobjects["sphere"]->colour = glm::vec4(1.0f);


	float totalWidth = 240;
	float brickHeight = 8.0f;

	// Generate a bunch of objects in a circle
	int numLayers = 6;
	int numBricks = 10;
	float brickWidth = totalWidth / (float)numBricks;

	float brickTop = 0.0f;
	float brickLeft = -totalWidth / 2.0f;

	for (int layer = 0; layer < numLayers; layer++) {
		for (int brick = 0; brick < numBricks; brick++)
		{
			glm::vec3 pos, colour = glm::vec3(0.0f, 1.0f, 0.0f);
			std::string name = ("brick" + std::to_string(layer) + "_" + std::to_string(brick));
			pos.x = brickLeft + brick * brickWidth + brick;
			pos.y = 10.0f;
			pos.z = layer + brickTop + layer * brickHeight;
			gameobjects[name] = std::make_shared<GameObject>(pos, meshes["cube"], defaultMaterial);
			gameobjects[name]->setScale(glm::vec3(brickWidth, 1.0f, brickHeight));
			gameobjects[name]->colour = glm::vec4(colour, 1.0f);
		}
	}
}

void updateScene()
{
	// Move light in simple circular path
	static float ang = 0.0f;

	if (!paused)
		ang += deltaTime; // comment out to pause light
	const float radius = 15.0f;
	lightPos.x = cos(ang) * radius;
	lightPos.y = cos(ang*4.0f) * 2.0f + 15.0f;
	lightPos.z = sin(ang) * radius;
	lightPos.w = 1.0f;

	gameobjects["sphere"]->setPosition(lightPos);

	// Update all game objects
	for (auto itr = gameobjects.begin(); itr != gameobjects.end(); ++itr)
	{
		auto gameobject = itr->second;

		// Remember: root nodes are responsible for updating all of its children
		// So we need to make sure to only invoke update() for the root nodes.
		// Otherwise some objects would get updated twice in a frame!
		if (gameobject->isRoot())
			gameobject->update(deltaTime);
	}
}

void drawScene(TTK::Camera& cam)
{
	for (auto itr = gameobjects.begin(); itr != gameobjects.end(); ++itr)
	{
		auto gameobject = itr->second;

		if (gameobject->isRoot())
			gameobject->draw(cam);
	}
}

// Helpful function to apply a shader program on all objects
void setMaterialForAllGameObjects(std::string materialName)
{
	auto mat = materials[materialName];
	for (auto itr = gameobjects.begin(); itr != gameobjects.end(); ++itr)
	{
		itr->second->material = mat;
	}
}

// This is where we draw stuff
void DisplayCallbackFunction(void)
{
	// Clear screen
	glClearColor(0.8, 0.8, 0.8, 1.0);
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	// Update cameras
	playerCamera.update();

	// Update all gameobjects
	updateScene();

	// Set material properties

	// This framework uses the idea of "materials", see Material.h
	// A material is just a shader program + some data structures to hold memory for the uniform values
	// See initializeScene() to see how GameObjects are created. Currently all GameObjects are using the
	// same material reference (all GameObjects are currently pointing to the same material).
	// This means when we modify the material properties for "default", all of the objects will be effected.
	auto defaultMaterial = materials["default"];

	// Lighting calculations can be performed in ANY space you like (local, world, eye, screen etc.) you just need to make sure
	// that everything (surface point, light positon, surface normal) are in the same space. 
	// This sample performs lighting in eye space.
	defaultMaterial->vec4Uniforms["u_lightPos"] = playerCamera.viewMatrix * lightPos; // EYE SPACE	
	// draw the scene
	if (true) // lambert
	{
		setMaterialForAllGameObjects("default");
		drawScene(playerCamera);
	}

	TTK::StartUI(windowWidth, windowHeight);
	ImGui::Checkbox("Animate Light", &paused);
	TTK::EndUI();

	/* Swap Buffers to Make it show up on screen */
	glutSwapBuffers();
}

/* function void KeyboardCallbackFunction(unsigned char, int,int)
* Description:
*   - this handles keyboard input when a button is pressed
*/
void KeyboardCallbackFunction(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27: // the escape key
		glutExit();
	break;	
	case '1':
		mode = 0;
		break;
	case '2':
		mode = 1;
		break;
	case '3':
		mode = 2;
		break;
	}

	// Imgui

	ImGuiIO& io = ImGui::GetIO();
	io.KeysDown[(int)key] = true;
	io.AddInputCharacter((int)key); // this is what makes keyboard input work in imgui

	// This is what makes the backspace button work
	int keyModifier = glutGetModifiers();
	switch (keyModifier)
	{
	case GLUT_ACTIVE_SHIFT:
		io.KeyShift = true;
		break;

	case GLUT_ACTIVE_CTRL:
		io.KeyCtrl = true;
		break;

	case GLUT_ACTIVE_ALT:
		io.KeyAlt = true;
		break;
	}
}

/* function void KeyboardUpCallbackFunction(unsigned char, int,int)
* Description:
*   - this handles keyboard input when a button is lifted
*/
void KeyboardUpCallbackFunction(unsigned char key, int x, int y)
{
	// Imgui

	ImGuiIO& io = ImGui::GetIO();
	io.KeysDown[key] = false;

	int keyModifier = glutGetModifiers();
	io.KeyShift = false;
	io.KeyCtrl = false;
	io.KeyAlt = false;
	switch (keyModifier)
	{
	case GLUT_ACTIVE_SHIFT:
		io.KeyShift = true;
		break;

	case GLUT_ACTIVE_CTRL:
		io.KeyCtrl = true;
		break;

	case GLUT_ACTIVE_ALT:
		io.KeyAlt = true;
		break;
	}
}

/* function TimerCallbackFunction(int value)
* Description:
*  - this is called many times per second
*  - this enables you to animate things
*  - no drawing, just changing the state
*  - changes the frame number and calls for a redisplay
*  - FRAME_DELAY is the number of milliseconds to wait before calling the timer again
*/
void TimerCallbackFunction(int value)
{
	// Calculate new deltaT for potential updates and physics calculations
	static int elapsedTimeAtLastTick = 0;
	int totalElapsedTime = glutGet(GLUT_ELAPSED_TIME);

	deltaTime = float(totalElapsedTime - elapsedTimeAtLastTick);
	deltaTime /= 1000.0f;
	elapsedTimeAtLastTick = totalElapsedTime;

	/* this call makes it actually show up on screen */
	glutPostRedisplay();
	/* this call gives it a proper frame delay to hit our target FPS */
	glutTimerFunc(FRAME_DELAY, TimerCallbackFunction, 0);
}

/* function WindowReshapeCallbackFunction()
* Description:
*  - this is called whenever the window is resized
*  - and sets up the projection matrix properly
*/
void WindowReshapeCallbackFunction(int w, int h)
{
	/* Update our Window Properties */
	windowWidth = w;
	windowHeight = h;

	if (renderBuffer)
		renderBuffer->Resize(w, h);

	playerCamera.winHeight = (float)h;
	playerCamera.winWidth = (float)w;

	glViewport(0, 0, w, h);
}


void MouseClickCallbackFunction(int button, int state, int x, int y)
{
	mousePosition.x = (float)x;
	mousePosition.y = (float)y;

	mousePositionFlipped = mousePosition;
	mousePositionFlipped.y = windowHeight - mousePosition.y;

	ImGui::GetIO().MouseDown[0] = !state;
}

void SpecialInputCallbackFunction(int key, int x, int y)
{
	switch (key)
	{
	case GLUT_KEY_UP:
		position.z += movementSpeed * deltaTime;
		break;
	case GLUT_KEY_DOWN:
		position.z -= movementSpeed * deltaTime;
		break;
	case GLUT_KEY_LEFT:
		position.x += movementSpeed * deltaTime;
		break;
	case GLUT_KEY_RIGHT:
		position.x -= movementSpeed * deltaTime;
		break;
	}
}

// Called when the mouse is clicked and moves
void MouseMotionCallbackFunction(int x, int y)
{
	ImGui::GetIO().MousePos = ImVec2((float)x, (float)y);

	if (!ImGui::GetIO().WantCaptureMouse)
	{
		if (mousePosition.length() > 0)
			playerCamera.processMouseMotion(x, y, (int)mousePosition.x, (int)mousePosition.y, deltaTime);
	}

	mousePosition.x = (float)x;
	mousePosition.y = (float)y;

	mousePositionFlipped = mousePosition;
	mousePositionFlipped.y = windowHeight - mousePosition.y;

}

// Called when the mouse is moved inside the window
void MousePassiveMotionCallbackFunction(int x, int y)
{
	ImGui::GetIO().MousePos = ImVec2((float)x, (float)y);

	mousePositionFlipped.x = (float)x;
	mousePositionFlipped.y = (float)(windowHeight - y);
}

/* function main()
* Description:
*  - this is the main function
*  - does initialization and then calls glutMainLoop() to start the event handler
*/
int main(int argc, char **argv)
{
	// Memory Leak Detection
#ifdef _DEBUG
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	/* initialize the window and OpenGL properly */

	//////////////////////////////////////////////////////////////////////////
	// NOTE:
	// Must set a CORE_PROFILE for render doc to work
	// Must use FREEGLUT instead of GLUT
	//////////////////////////////////////////////////////////////////////////
	glutInitContextVersion(4, 0);
	glutInitContextProfile(GLUT_CORE_PROFILE);
	glutInit(&argc, argv);
	glutInitWindowSize(windowWidth, windowHeight);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
	glutCreateWindow("Tutorial");

	auto s = glGetString(GL_VERSION);
	std::cout << s << std::endl;

	/* set up our function callbacks */
	glutDisplayFunc(DisplayCallbackFunction);
	glutKeyboardFunc(KeyboardCallbackFunction);
	glutKeyboardUpFunc(KeyboardUpCallbackFunction);
	glutReshapeFunc(WindowReshapeCallbackFunction);
	glutMouseFunc(MouseClickCallbackFunction);
	glutMotionFunc(MouseMotionCallbackFunction);
	glutTimerFunc(1, TimerCallbackFunction, 0);
	glutSpecialFunc(SpecialInputCallbackFunction);
	glutPassiveMotionFunc(MousePassiveMotionCallbackFunction);


	// Init GLEW
	GLenum err = glewInit();
	if (err != GLEW_OK)
	{
		std::cout << "TTK::InitializeTTK Error: GLEW failed to init" << std::endl;
	}
	printf("OpenGL version: %s, GLSL version: %s\n", glGetString(GL_VERSION), glGetString(GL_SHADING_LANGUAGE_VERSION));

	// Init ImGUI
	TTK::InitImGUI();

	int num_ext = 0;
	glGetIntegerv(GL_NUM_EXTENSIONS, &num_ext);
	for (int i = 0; i < num_ext; i++)
	{
		const char* str = (const char*)glGetStringi(GL_EXTENSIONS, i);
		if (!strcmp(str, "GL_ARB_compatibility"))
			printf("Compatiblity Profile! RENDER DOC WILL NOT WORK!!\n");
	}

	// Init GL
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	// Initialize scene
	initializeShaders();
	initializeScene();

	/* Start Game Loop */
	deltaTime = (float)glutGet(GLUT_ELAPSED_TIME);
	deltaTime /= 1000.0f;

	glutMainLoop();

	delete renderBuffer;

	return 0;
}